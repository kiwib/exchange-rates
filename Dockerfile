# BUILD FRONTEND
FROM node:lts-alpine as build-stage

MAINTAINER Andrey Shaikin <kiwibon@yandex.ru>

RUN true \
    && mkdir -p /opt/exchange-rates \
    && apk add --no-cache nodejs yarn python3 make g++

WORKDIR /opt/exchange-rates
COPY package.*json webpack.config.js yarn.lock ./
COPY src ./src
COPY static ./static/

RUN true \
    && mkdir -p static \
     && yarn install --no-progress --non-interactive \
     && yarn cache clean \
     && yarn build

# production stage
FROM nginx:stable-alpine as production-stage
# очищаем от стандартных данных
RUN rm -rf /usr/share/nginx/html/
RUN rm -rf /etc/nginx/conf.d

COPY --from=build-stage /opt/exchange-rates/dist /usr/share/nginx/html
COPY --from=build-stage /opt/exchange-rates/static /usr/share/nginx/html/static

COPY index.html /usr/share/nginx/html/
COPY favicon.ico /usr/share/nginx/html/
RUN ls -la /usr/share/nginx/html/

COPY deploy/conf/conf.d/* /etc/nginx/conf.d/

EXPOSE 80
VOLUME ["/var/log", "/usr/share/nginx/html/static"]
CMD ["nginx", "-g", "daemon off;"]
