const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: path.join(__dirname, 'src/main.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'dist/[name].build.js',
  },
  module: {
    rules: [{
      test: /\.vue$/,
      loader: 'vue-loader'
    }, {
      test: /\.css$/,
      use: [
        'style-loader',
        'vue-style-loader',
        'css-loader',
      ]
    }, {
      test: /\.pug$/,
      loader: 'pug-plain-loader'
    }]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin()
  ],
  devServer: {
    host: 'localhost',
    port: 3003
  },
  resolve: {
    alias: {
      apps: path.join(__dirname, 'src', 'apps')
    },
    extensions: ['.js', '.vue', '.css']
  }
}
