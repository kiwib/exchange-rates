import Vue from 'vue'
import App from './App'
import vuetify from './plugins/vuetify'
import store from './store'
import router from './router'

/* PLUGIN */
import ajax from './plugins/ajax'
import moment from './plugins/moment'
import apiPath from './plugins/apiPath'
import logError from './plugins/logError'
import lodash from './plugins/lodash'

Vue.use(apiPath)
Vue.use(ajax)
Vue.use(moment)
Vue.use(lodash)
Vue.use(logError, {store})
/* ##### */

/* STYLE */
import './style'
/* ##### */

new Vue({
  el: '#app',
  vuetify,
  store,
  router,
  render: h => h(App)
})
