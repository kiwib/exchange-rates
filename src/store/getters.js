export default {
  AuthorData(state) {
    return state.AUTHOR_DATA
  },
  loggedErrors(state) {
    return state.LOGGED_ERROR
  }
}
