import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
import LazyComponents from 'apps/common/LazyComponents'

export default new VueRouter({
  routes: [
    {
      name: 'exchange-rate',
      path: '/exchange-rate',
      component: LazyComponents.ExchangeRate
    }, {
    name: 'exchange-number-systems',
      path: '/exchange-number-systems',
      component: LazyComponents.ExchangeNumberSystems
    }]
})
