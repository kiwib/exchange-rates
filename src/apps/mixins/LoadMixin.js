export default {
  data() {
    return {
      loading: true,
      loaded: false
    }
  },
  methods: {
    load() {
      this.beforeLoad()
      this.load()
      this.afterLoad()
    },
    beforeLoad() {
      this.loaded = false
      this.loading = true
    },
    afterLoad() {
      this.loaded = true
      this.loading = false
    }
  },
  mounted() { this.load() }
}
