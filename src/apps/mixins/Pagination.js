export default {
  data() {
    return {
      currentPage: 1,
      perPage: 5,
      totalVisible: 10,
      pages: [],
      currentPageItems: []
    }
  },
  computed: {
    pageCount() {
      return this.pages.length
    }
  },
  watch: {
    currentPage(newVal) {
      if (newVal) {
        this.currentPageItems = this.pages[newVal === 0 ? 0 : newVal - 1]
      }
    }
  }
}
