export default {
  CustomFooter: () => import(/* webpackChunkName 'custom-footer' */ 'apps/components/CustomFooter'),
  CustomMain: () => import(/* webpackChunkName 'custom-main' */ 'apps/components/CustomMain'),
  Exchange: () => import(/* webpackChunkName 'exchange' */ 'apps/components/Exchange'),
}
