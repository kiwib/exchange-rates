export default {
  install(Vue, {store}) {
    Vue.prototype.$logError = error => {
      store.dispatch('addError', {error})
    }
  }
}
