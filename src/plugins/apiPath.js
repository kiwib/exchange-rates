import moment from 'moment'

export default {
    install(Vue, options) {
        const dateFormat = 'DD/MM/YYYY'
        const currentDate = moment().format(dateFormat)
        const proxy = 'https://cors-anywhere.shaykin-av.ru/'
        const url = 'https://www.cbr.ru/scripts/XML_daily.asp?date_req='
        Vue.prototype.$apiPath = `${proxy}${url}${currentDate}`
    }
}
